/**
 * View Models used by Spring MVC REST controllers.
 */
package br.ufc.quixada.gc.web.rest.vm;
