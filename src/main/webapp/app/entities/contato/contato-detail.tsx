import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './contato.reducer';
import { IContato } from 'app/shared/model/contato.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IContatoDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ContatoDetail = (props: IContatoDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { contatoEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="contatosApp.contato.detail.title">Contato</Translate> [<b>{contatoEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="nome">
              <Translate contentKey="contatosApp.contato.nome">Nome</Translate>
            </span>
          </dt>
          <dd>{contatoEntity.nome}</dd>
          <dt>
            <span id="email">
              <Translate contentKey="contatosApp.contato.email">Email</Translate>
            </span>
          </dt>
          <dd>{contatoEntity.email}</dd>
          <dt>
            <span id="fone">
              <Translate contentKey="contatosApp.contato.fone">Fone</Translate>
            </span>
          </dt>
          <dd>{contatoEntity.fone}</dd>
        </dl>
        <Button tag={Link} to="/contato" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/contato/${contatoEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ contato }: IRootState) => ({
  contatoEntity: contato.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ContatoDetail);
