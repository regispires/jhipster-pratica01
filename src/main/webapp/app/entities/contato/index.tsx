import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Contato from './contato';
import ContatoDetail from './contato-detail';
import ContatoUpdate from './contato-update';
import ContatoDeleteDialog from './contato-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ContatoUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ContatoUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ContatoDetail} />
      <ErrorBoundaryRoute path={match.url} component={Contato} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ContatoDeleteDialog} />
  </>
);

export default Routes;
