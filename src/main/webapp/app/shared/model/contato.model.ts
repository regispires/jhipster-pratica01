export interface IContato {
  id?: number;
  nome?: string;
  email?: string;
  fone?: string;
}

export const defaultValue: Readonly<IContato> = {};
