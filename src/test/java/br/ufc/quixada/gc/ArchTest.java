package br.ufc.quixada.gc;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("br.ufc.quixada.gc");

        noClasses()
            .that()
                .resideInAnyPackage("br.ufc.quixada.gc.service..")
            .or()
                .resideInAnyPackage("br.ufc.quixada.gc.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..br.ufc.quixada.gc.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
